import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import requests
import json

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker

# make a function to get the temperatures of a given location
def getTemperatures(locations, location, start_date, end_date):
    # get the lat and long of the location
    lat, lon = locations[location]
    req = requests.get(f"https://archive-api.open-meteo.com/v1/era5?latitude={lat}&longitude={lon}&start_date={start_date}&end_date={end_date}&hourly=temperature_2m" )
    req = json.loads(req.text)
    # get the tempratures
    temperatures = req['hourly']['temperature_2m']
    # get the times
    times = req['hourly']['time']
    return times, temperatures

# create a function for the net present value
def net_present_value(cash_flow, T: int, interest_rate: float):
    
    if isinstance(cash_flow, list):
        return sum(cash_flow[t]/(1+interest_rate)**t for t in range(T))
    else:
        return sum(cash_flow/(1+interest_rate)**t for t in range(T))
    
# create a function for the present value factor
def present_value_factor(T: int, interest_rate: float):
    return ((1+interest_rate)**T - 1) / ((1+interest_rate)**T * interest_rate)

## Task 2.0
# Write the function to calculte the annuity factor.
def annuity_factor(i: float, T: int):
    return ((1+i)**T-1)/((1+i)**T)*i

## Task 4.11
def plot_electricity_prices(prices: pd.Series()):

    # Assuming your pandas series is named 'prices'
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(10, 5), gridspec_kw={'width_ratios': [3, 2]})

    # Create the stair plot on the left axis
    ax1.step(prices.index, prices.values)

    # Add labels and title to the left axis
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Price')
    ax1.set_title('Day Ahead Prices in the German Electricity Market')

    # Format the x-axis to show only the hours
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    ax1.xaxis.set_major_locator(mdates.HourLocator(interval=2))

    # Add both major and minor grids to the histogram plot
    ax1.yaxis.grid(True, which='major', linestyle='--', color='black', alpha=0.5)
    ax1.yaxis.grid(True, which='minor', linestyle=':', color='black', alpha=0.3)

    # Calculate and plot the maximum, minimum, and average values on the left axis
    max_val = prices.max()
    min_val = prices.min()
    avg_val = prices.mean()
    ax1.axhline(max_val, color='r', linestyle='--', label='Maximum')
    ax1.axhline(min_val, color='g', linestyle='--', label='Minimum')
    ax1.axhline(avg_val, color='b', linestyle='--', label='Average')
    ax1.legend(loc="upper left")

    # Annotate the maximum, minimum, and average values with text labels on the left axis
    ax1.annotate(f'{max_val:.2f}', xy=(prices.index[-1], max_val), xytext=(-20, -15), 
                textcoords='offset points', color='r', fontsize=10)
    ax1.annotate(f'{min_val:.2f}', xy=(prices.index[-1], min_val), xytext=(-20, 5), 
                textcoords='offset points', color='g', fontsize=10)
    ax1.annotate(f'{avg_val:.2f}', xy=(prices.index[-1], avg_val), xytext=(-20, -15), 
                textcoords='offset points', color='b', fontsize=10)

    # Rotate the x-axis labels on the left axis for better readability
    plt.setp(ax1.xaxis.get_majorticklabels(), rotation=90)

    # Create the histogram on the right axis
    n_bins = 30
    hist, bins, patches = ax2.hist(prices, bins=n_bins, orientation='horizontal', color='gray', alpha=0.5)
    ax2.axhline(avg_val, color='b', linestyle='--', label='Average')
    ax2.axhline(avg_val + prices.std(), color='r', linestyle='--', label='Standard deviation')
    ax2.axhline(avg_val - prices.std(), color='r', linestyle='--')
    ax2.set_xlabel('Frequency')
    ax2.set_ylabel('Price')
    ax2.set_title('Price Distribution')
    ax2.legend(loc="upper right")

    # Set the x-axis ticks on the histogram plot to integers
    ax2.xaxis.set_major_locator(ticker.FixedLocator(np.arange(0, max(hist) + 1)))
    ax2.xaxis.set_major_formatter(ticker.FixedFormatter(np.arange(0, max(hist) + 1, dtype=int)))

    # Add both major and minor grids to the histogram plot
    ax2.xaxis.grid(True, which='major', linestyle='--', color='black', alpha=0.5)
    ax2.xaxis.grid(True, which='minor', linestyle=':', color='black', alpha=0.3)

    # Show the plot
    plt.show()
    