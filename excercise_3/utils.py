import requests
import json

# make a function to get the temperatures of a given location
def getTemperatures(locations, location, start_date, end_date):
    # get the lat and long of the location
    lat, lon = locations[location]
    req = requests.get(f"https://archive-api.open-meteo.com/v1/era5?latitude={lat}&longitude={lon}&start_date={start_date}&end_date={end_date}&hourly=temperature_2m" )
    req = json.loads(req.text)
    # get the tempratures
    temperatures = req['hourly']['temperature_2m']
    # get the times
    times = req['hourly']['time']
    return times, temperatures

# create a function for the net present value
def net_present_value(cash_flow, T: int, interest_rate: float):
    
    if isinstance(cash_flow, list):
        return sum(cash_flow[t]/(1+interest_rate)**t for t in range(T))
    else:
        return sum(cash_flow/(1+interest_rate)**t for t in range(T))
    
# create a function for the present value factor
def present_value_factor(T: int, interest_rate: float):
    return ((1+interest_rate)**T - 1) / ((1+interest_rate)**T * interest_rate)

## Task 2.0
# Write the function to calculte the annuity factor.
def annuity_factor(i: float, T: int):
    return ((1+i)**T-1)/((1+i)**T)*i